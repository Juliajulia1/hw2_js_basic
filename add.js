//Практичні завдання
//1. Створіть змінну "username" і присвойте їй ваше ім'я. Створіть змінну "password" і присвойте їй пароль (наприклад, "secret").
//Далі ми імітуємо введеня паролю користувачем.
   // Отримайте від користувача значення його паролю і перевірте, чи співпадає воно зі значенням в змінній "password".
   // Виведіть результат порівнння в консоль.
let username = 'Julia';
let password = 'secret';
let enterPassword = prompt(`Please ${username}, enter your password:`);
console.log(password === enterPassword);

//Створіть змінну x та присвойте їй значення 5. Створіть ще одну змінну y та запишіть присвойте їй значення 3.
//Виведіть результати додавання, віднімання, множення та ділення
//змінних x та y у вікні alert.

let x = 5;
let y = 3;
let sum = x+y;
let subtraction = x-y;
let multiplied = x * y;
let divided = x/y;
alert(`addition of ${x}+${y} = ${sum}`);
alert (`Subtraction of ${x}-${y} = ${subtraction}`);
alert(`Multiplied of ${x}*${y} = ${multiplied}`)
alert(`Divided of ${x}/${y} = ${divided}`);